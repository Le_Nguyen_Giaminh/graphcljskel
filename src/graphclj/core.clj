(ns graphclj.core
  (:require [graphclj.graph :as graph]
            [graphclj.tools :as tools]
            [graphclj.centrality :as central]
            [clojure.string :as str])
  (:gen-class))

(defn -main 
  [& args]
  (do
    (println "Bienvenue au générateur des graphes. Saisissez : \n aleatoire : si vous voulez génerer un graphe aléatoire avec l'algorithme erdos-renyi \n
import : si vous voulez génerer un graphe à partir d'un fichier \n quit : si vous voulez quitter le programme")
    (let [rep (read-line)]
      (cond
        (= "quit" rep) (do
                         (println "\nMerci. Au revoir.\n")
                         (System/exit 0))
        (= "aleatoire" rep) (do
                              (println "Entrez le nombre des noeuds et la probabilité d'avoir un lien entre une paire de noeuds \n Ex : 5 0.5")
                              (let [input (str/split (read-line) #" ")
                                    g (graph/erdos-renyi-rnd (Integer. (first input)) (Double. (second input)))]
                                (do
                                  (flush)
                                  (println g)
                                  (println "Tapez degre pour obtenir le graph complet avec leur degrés\ncentral pour obtenir le graph complet 
avec leur centralité.")
                                  (let [rep (read-line)]
                                    (if (= "degre" rep)
                                      (let [g (tools/rank-nodes (central/closeness-all g) :close)]
                                        (do
                                          (flush)
                                          (println g)
                                          (println "Tapez le chemin vers un fichier pour sauvegarder le graph sous le format de .dot. 
Sinon tapez quit pour quitter le programme")
                                          (let [rep (read-line)]
                                            (if (= rep "quit")
                                              (do
                                                (println "\nMerci. Au revoir.\n")
                                                (System/exit 0))
                                              (do
                                                (flush)
                                                (tools/writefile rep (tools/to-dot g))
                                                (println "Résultat a été écrit dans le fichier : " rep)
                                                (println "\nMerci. Au revoir.\n")
                                                (System/exit 0))))))
                                      (let [g (tools/rank-nodes (central/closeness-all g) :degree)]
                                        (do
                                          (flush)
                                          (println g)
                                          (println "Tapez le chemin vers un fichier pour sauvegarder le graph sous le format de .dot. 
Sinon tapez quit pour quitter le programme")
                                          (let [rep (read-line)]
                                            (if (= rep "quit")
                                              (do
                                                (println "\nMerci. Au revoir.\n")
                                                (System/exit 0))
                                              (do
                                                (flush)
                                                (tools/writefile rep (tools/to-dot g))
                                                (println "Résultat a été écrit dans le fichier : " rep)
                                                (println "\nMerci. Au revoir.\n")
                                                (System/exit 0)))))))))))
        :else (do
                (println "Tapez le chemin du fichier d'entrée")
                (let [rep (read-line)
                      g (graph/gen-graph (tools/readfile rep))]
                  (do
                    (flush)
                    (println g)
                    (println "Tapez degre pour obtenir le graph complet avec leur degrés\ncentral pour obtenir le graph complet 
avec leur centralité.")
                    (let [rep (read-line)]
                      (if (= "degre" rep)
                        (let [g (tools/rank-nodes (central/closeness-all g) :close)]
                          (do
                            (flush)
                            (println g)
                            (println "Tapez le chemin vers un fichier pour sauvegarder le graph sous le format de .dot. 
Sinon tapez quit pour quitter le programme")
                            (let [rep (read-line)]
                              (if (= rep "quit")
                                (do
                                  (println "\nMerci. Au revoir.\n")
                                  (System/exit 0))
                                (do
                                  (flush)
                                  (tools/writefile rep (tools/to-dot g))
                                  (println "Résultat a été écrit dans le fichier : " rep)
                                  (println "\nMerci. Au revoir.\n")
                                  (System/exit 0))))))
                        (let [g (tools/rank-nodes (central/closeness-all g) :degree)]
                          (do
                            (flush)
                            (println g)
                            (println "Tapez le chemin vers un fichier pour sauvegarder le graph sous le format de .dot. 
Sinon tapez quit pour quitter le programme")
                            (let [rep (read-line)]
                              (if (= rep "quit")
                                (do
                                  (println "\nMerci. Au revoir.\n")
                                  (System/exit 0))
                                (do
                                  (flush)
                                  (tools/writefile rep (tools/to-dot g))
                                  (println "Résultat a été écrit dans le fichier : " rep)
                                  (println "\nMerci. Au revoir.\n")
                                  (System/exit 0))))))))))))))
)
