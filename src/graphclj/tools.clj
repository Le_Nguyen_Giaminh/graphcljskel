(ns graphclj.tools
  (:require [clojure.string :as str]
            [graphclj.centrality :as central])
  (:use midje.sweet))

(defn readfile [f]
  "Returns a sequence from a file f"
  (with-open [rdr (clojure.java.io/reader f)]
    (doall (line-seq rdr))))

(defn writefile [f str]
  (with-open [wrtr (clojure.java.io/writer f)]
    (.write wrtr str)))

(defn rank [g label noeud]
  "Calculer le rank d'un noeud"
  (loop [clefs (keys g), r 0]
    (if (seq clefs)
      (if (> (get (get g noeud) label) (get (get g (first clefs)) label))
        (recur (rest clefs) (inc r))
        (recur (rest clefs) r))
      r))
  )

;; Test rank
(fact 
 (let [g {1 {:neigh #{0 4 3}, :close 3.5}
          0 {:neigh #{1 3}, :close 3.0}
          3 {:neigh #{0 1 2}, :close 3.5}
          4 {:neigh #{1}, :close 2.333333343267441}
          2 {:neigh #{3}, :close 2.333333343267441}}]
   (rank g :close 2))
 => 0)

(defn rank-nodes [g,l]
  "Ranks the nodes of the graph in relation to label l in accending order"
  (loop [clefs (keys g), res {}]
    (if (seq clefs)
      (recur (rest clefs) (assoc res (first clefs) (assoc (get g (first clefs)) :rank (rank g l (first clefs)))))
      res)))

;; Test rank-nodes
(fact
 (let [g {1 {:neigh #{0 4 3}, :close 3.5}
          0 {:neigh #{1 3}, :close 3.0}
          3 {:neigh #{0 1 2}, :close 3.5}
          4 {:neigh #{1}, :close 2.333333343267441}
          2 {:neigh #{3}, :close 2.333333343267441}}]
   (rank-nodes g :close)) 
 => {1 {:neigh #{0 4 3}, :close 3.5, :rank 3}
     0 {:neigh #{1 3}, :close 3.0, :rank 2}
     3 {:neigh #{0 1 2}, :close 3.5, :rank 3}
     4 {:neigh #{1}, :close 2.333333343267441, :rank 0}
     2 {:neigh #{3}, :close 2.333333343267441, :rank 0}})

(defn generate-colors [n]
    (let [step 10]
    (loop [colors {}, current [255.0 160.0 122.0], c 0]
      (if (= c (inc n))
        colors
        (recur (assoc colors c (map #(/ (mod (+ step %) 255) 255) current))
               (map #(mod (+ step %) 255) current) (inc c))
      ))))

(defn parse-int [s]
  (Integer. (re-find  #"\d+" s)))

(defn arete [g noeud res]
  "Trouve tous les arêtes à partir d'un noeuds"
  (loop [neighbors (get (get g noeud) :neigh), res res]
    (if (seq neighbors)
      (if (> noeud (first neighbors))
        (recur (rest neighbors) (conj res [(first neighbors) noeud]))
        (recur (rest neighbors) (conj res [noeud (first neighbors)])))
      res)))

;; Test arete
(fact 
 (let [g {1 {:neigh #{0 4 3}, :close 3.5}
          0 {:neigh #{1 3}, :close 3.0}
          3 {:neigh #{0 1 2}, :close 3.5}
          4 {:neigh #{1}, :close 2.333333343267441}
          2 {:neigh #{3}, :close 2.333333343267441}}]
   (arete g 1 #{})) 
 => #{[1 4] [1 3] [0 1]})

(defn all-arete [g]
  "Trouve tous les arêtes d'un graph"
  (loop [clefs (keys g), res #{}]
    (if (seq clefs)
      (recur (rest clefs) (clojure.set/union res (arete g (first clefs) res)))
      res)))

;; Test all-arete
(fact 
 (let [g {1 {:neigh #{0 4 3}, :close 3.5}
          0 {:neigh #{1 3}, :close 3.0}
          3 {:neigh #{0 1 2}, :close 3.5}
          4 {:neigh #{1}, :close 2.333333343267441}
          2 {:neigh #{3}, :close 2.333333343267441}}]
   (all-arete g))
 => #{[2 3] [1 4] [1 3] [0 3] [0 1]})

(defn arete-to-string [g]
  (loop [aretes (all-arete g), res ""]
    (if (seq aretes)
      (recur (rest aretes) (str res (ffirst aretes) " -- " (second (first aretes)) "\n"))
      res)))

;; Test arete-to-string
(let [g {1 {:neigh #{0 4 3}, :close 3.5}
         0 {:neigh #{1 3}, :close 3.0}
         3 {:neigh #{0 1 2}, :close 3.5}
         4 {:neigh #{1}, :close 2.333333343267441}
         2 {:neigh #{3}, :close 2.333333343267441}}]
  (arete-to-string g))

(defn rank-max [g]
  (loop [clefs (keys g), res (get (get g (first clefs)) :rank)]
    (if (seq clefs)
      (if (> (get (get g (first clefs)) :rank) res)
        (recur (rest clefs) (get (get g (first clefs)) :rank))
        (recur (rest clefs) res))
      res)))

(defn color-node-to-string [g]
  (let [couleurs (generate-colors (rank-max g))]
    (loop [clefs (keys g), res ""]
      (if (seq clefs)
        (let [noeud (first clefs)
              color (get couleurs (get (get g noeud) :rank))
              r (nth color 0)
              g (nth color 1)
              b (nth color 2)]
          (recur (rest clefs) (str res noeud " [style=filled color=\"" r" " g " " b "\"]\n")))
        res))))

(let [g {1 {:neigh #{0 4 3}, :close 3.5, :rank 3}
         0 {:neigh #{1 3}, :close 3.0, :rank 2}
         3 {:neigh #{0 1 2}, :close 3.5, :rank 3}
         4 {:neigh #{1}, :close 2.333333343267441, :rank 0}
         2 {:neigh #{3}, :close 2.333333343267441, :rank 0}}]
  (color-node-to-string g))

(defn to-dot [g]
  "Returns a string in dot format for graph g, each node is colored in relation to its ranking"
  (str "graph g{\n" (color-node-to-string g) (arete-to-string g) "}"))
