(ns graphclj.graph
  (:require [clojure.string :as str])
  (:require [graphclj.tools :as tools]))

;; Generate a graph from the lines
(defn gen-graph [lines]
  "Returns a hashmap contating the graph"
  (loop [s lines, res {}]
    (if (seq s)
      (let [l (str/split (first s) #" ")
            x (tools/parse-int (first l))
            y (tools/parse-int (second l))]
        (if (contains? res x)
          (let [v (get res x)
                n (get v :neigh)
                new_v (assoc v :neigh (conj n y))]
            (recur (rest s) (assoc res x new_v)))
          (recur (rest s) (assoc res x {:neigh #{y}}))))
      res)))
 
;; Test gen-graph avec enron_static.csv
(gen-graph (tools/readfile (-> (java.io.File. "src/graphclj/enron_static.csv") .getAbsolutePath))) 

(defn erdos-renyi-rnd [n,p]
  "Returns a G_{n,p} random graph, also known as an Erdős-Rényi graph"
  (loop [i 0, noeuds []]
    (if (< i n)
      (recur (inc i) (conj noeuds i))
      (loop [noeuds noeuds, res []]
        (if (seq noeuds)
          (let [v (loop [r_noeuds (rest noeuds), res res]
                    (if (seq r_noeuds)
                      (recur (rest r_noeuds) (if (< (rand) p)
                                               (conj res (str (first noeuds) " " (first r_noeuds)))
                                               res))
                      res))]
            (recur (rest noeuds) v))
          (gen-graph res))))))

;; Test erdos-renyi-rnd
(erdos-renyi-rnd 5 0.5) 
