(ns graphclj.centrality
    (:require [graphclj.graph :as graph]
              [clojure.set :as set]))

(defn degrees [g]
  "Calculates the degree centrality for each node"
  (let [clefs (keys g)]
    (loop [clefs clefs, g g]
      (if (seq clefs)
        (let [v (get g (first clefs))
              nbr (count (get v :neigh))
              new_v (assoc v :degree nbr)]
          (recur (rest clefs) (assoc g (first clefs) new_v)))
        g))))

;; Test degrees
(let [g (graph/erdos-renyi-rnd 5 0.5)]
  (degrees g))

(defn get-nodes-from [g n]
  "Retourne tous les noeuds qui sont liés aux noeuds dans n"
  (loop [noeuds n, res #{}]
    (if (seq noeuds)
      (recur (rest noeuds) (set/union res (get (get g (first noeuds)) :neigh)))
      res)))

;; Test get-nodes-from
(get-nodes-from {1 {:neigh #{2 3}} 2 {:neigh #{0}}} [1])

(defn add-nodes [noeuds noeuds_a_ajouter dist]
  "Ajoute un noeuds dans un map des noeuds"
  (loop [noeuds noeuds, a_ajouter noeuds_a_ajouter]
    (if (seq a_ajouter)
      (if (contains? noeuds (first a_ajouter))
        (recur noeuds (rest a_ajouter))
        (recur (assoc noeuds (first a_ajouter) dist) (rest a_ajouter)))
      noeuds)))

;; Test add-nodes
(add-nodes {1 0.0 2 1.0} #{3} 3.0) 
 
(defn distance [g n]
  "Calculate the distances of one node to all the others"
  (loop [res (assoc {} n 0.0), dist 1.0, noeuds (set (list n))]
    (if (< (count noeuds) (count g))
      (let [noeuds_a_visiter (get-nodes-from g noeuds)]
        (recur (add-nodes res noeuds_a_visiter dist) (inc dist) (set/union noeuds noeuds_a_visiter)))
      res)))

;; Test distance
(let [g {1 {:neigh #{0 4 3}}
         0 {:neigh #{1 3}}
         3 {:neigh #{0 1 2}}
         4 {:neigh #{1}}
         2 {:neigh #{3}}}]
  (distance g 1))

(defn closeness [g n]
  "Returns the closeness for node n in graph g"
  (reduce + (map (fn [x] (if (== x 0.0) 0.0 (float (/ 1 x)))) (vals (distance g n)))))

;; Test closeness
(let [g {1 {:neigh #{0 4 3}}
         0 {:neigh #{1 3}}
         3 {:neigh #{0 1 2}}
         4 {:neigh #{1}}
         2 {:neigh #{3}}}]
  (closeness g 1))

(defn closeness-all [g]
  "Returns the closeness for all nodes in graph g"
  (loop [clefs (keys g), res {}]
    (if (seq clefs)
      (recur (rest clefs) (assoc res (first clefs) (assoc (get g (first clefs)) :close (closeness g (first clefs)))))
      res)))

;; Test closeness-all
(let [g {1 {:neigh #{0 4 3}}
         0 {:neigh #{1 3}}
         3 {:neigh #{0 1 2}}
         4 {:neigh #{1}}
         2 {:neigh #{3}}}]
  (closeness-all g))